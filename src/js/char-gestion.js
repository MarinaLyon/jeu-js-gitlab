import { run_ai_turn } from "./index.js";
import { clear_char_gamestate } from "./index.js";
import { select_chars_in_adjacency } from "./attack-system";

"use strict";

export let character_list = [];

export class Character {
    constructor(health, health_max, attack, x, y, team, ap, ap_max, name) {
        this.health = health;
        this.health_max = health_max;
        this.attack = attack;
        this.x = x;
        this.y = y;
        this.team = team;
        this.ap = ap;
        this.ap_max = ap_max;
        this.div = null;//je rajoute une propriété qui contiendra la div représentant le perso
        this.previousPos = { x: x, y: y };//je rajoute une propriété previousPos qui va valoir {x:x, y:y};
        this.name = name
    }

    doAttack(target) {
        //See attack-system.js for further code
        //condition cible "vivante" et attaquant vivant
        //valeur de vie enlevée égale au niveau d'attack du personnage qui attaque
        /*position : à côté de la cible :soit le x de la cible est égal au mien et le y= mon y + ou - 1
                                            soit le y de la cible est égal au mien et le x= mon x + ou - 1*/

        if (target.health > 0 && this.health > 0 && (target.x === this.x && (target.y === this.y + 1 || target.y === this.y - 1)) || (target.y === this.y && (target.x === this.x + 1 || target.x === this.x - 1))) {

            target.health -= this.attack;
            console.log(`${this} melee attacked ${target} for ${this.attack} HP (${target.health} HP remaining)`);

            if (target.health <= 0) {
                this.removeDeadChar(target);

            }
        }

        this.endTurn()
    }

    doRangedAttack(target) {
        target.health -= this.attack*0.5;
        console.log(`${this} ranged attacked ${target} for ${this.attack*0.5} HP (${target.health} HP remaining)`);
        this.endTurn()
    }

    move(keyCode) {
        this.previousPos.x = this.x;//j'assigne à this.previousPos.x la valeur de this.x et pareil pour y
        this.previousPos.y = this.y;
        if (keyCode === 38) {
            // up arrow
            this.y -= 1;
        }
        else if (keyCode === 40) {
            // down arrow//
            this.y += 1;
        }
        else if (keyCode === 37) {
            // left arrow
            this.x -= 1;
        }
        else if (keyCode === 39) {
            // right arrow
            this.x += 1;
        }
        this.ap--
        if(this.ap <= 0) {
            console.log("Action points exhausted. Turn ends");
            this.endTurn();
        }
    }

    /*eat() {  //ou eat(apple) et alors il faudra faire une classe items pour un inventaire
    if((this.y && this.x === apple.x && apple.y) && this.health < 100) {
        this.health += 30;
    }
    }*/


    removeDeadChar(target) {
        clear_char_gamestate(this);
        target.div.remove();
        character_list.splice(this);
    }

    endTurn() {
        this.ap = this.ap_max;
        if(this.team === "player") {
            run_ai_turn();
        }
        else {
            console.log("AI character done");
        }
    }

    run_ai() {
        console.log("INDIVIDUAL AI RUN")
        let adjacency_options = select_chars_in_adjacency(this);
        if (adjacency_options.length === 1) {
            this.doAttack(adjacency_options[0]);
        }
        else if (adjacency_options.length > 1) {
            let target = adjacency_options[Math.floor(Math.random() * adjacency_options.length)];
            this.doAttack(target);
        }
        else {
            let ranged_target_list = []
            for (let index = 0; index < character_list.length; index++) {
                if(character_list[index].team === "player"){
                    ranged_target_list.push(character_list[index]);
                }
            }
            let ranged_target = ranged_target_list[Math.floor(Math.random() * ranged_target_list.length)];
            this.doRangedAttack(ranged_target);
        }
    }

    log() {
        console.log(this);
    }
}